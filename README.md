# :microscope: Event Manager

 Gerenciador para eventos acadêmicos.

``` bash
git clone https://github.com/debora-rebelatto/event-manager
```

# :paperclip: Event Manager Back-End

## Requirements
For development, you will only need Node.js and a node global package, npm, installed in your environment.


## Install
``` bash
cd event-manager-back
npm install
```

## Running the project
``` bash
npm start
```
