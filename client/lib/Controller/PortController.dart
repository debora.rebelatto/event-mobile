import 'package:shared_preferences/shared_preferences.dart';

import '../ApiVars.dart';

class PortController {
  static dynamic updateHostPort({ String? url }) async {
    var pref = await SharedPreferences.getInstance();
    if(url != null) await pref.setString('url', url);
  }

  static Future<String> getHostPort() async {
    var pref = await SharedPreferences.getInstance();
    return pref.getString('url') ?? ApiVars.defaultIp;
  }
}
