import 'dart:convert';

import 'package:event_mobile/Services/UserServices.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SharedPrefController {
  static saveInfo(response) async {
    var decode = jsonDecode(response.body);
    var pref = await SharedPreferences.getInstance();

    await pref.setString('token', decode['token']);
    await pref.setString('userId', decode['user']['_id']);

    var res = await UserServices.userParticipant();
    await pref.setString('name', res['name']);
    await pref.setString('email', res['email']);
    await pref.setString('college', res['college'] ?? '');
    await pref.setBool('isOrganizer', res['isOrganizer'] != null ? res['isOrganizer'] : false);
  }

  static dynamic removeUser() async {
    var pref = await SharedPreferences.getInstance();

    await pref.remove('id');
    await pref.remove('token');
  }
}