import 'package:flutter/material.dart';

import 'Templates/InitialScreen/InitialScreen.dart';

Future main() async {
  runApp(MyApp());
}
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        appBarTheme: AppBarTheme(color: Colors.deepPurpleAccent[700]),
        inputDecorationTheme: InputDecorationTheme (
          labelStyle: TextStyle(color: Colors.deepPurpleAccent[700])
        ),

      ),
      home: InitialScreen(),
    );
  }
}

