import 'package:event_mobile/Requests.dart';

class ParticipantServices {
  static postParticipant(eventId) async {
    return Requests.httpPOST('/participant/$eventId');
  }
}