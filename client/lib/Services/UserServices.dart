import 'package:event_mobile/Requests.dart';

class UserServices {
  static userParticipant() async => Requests.httpGET('/user/info');

  static getEventsOrganizedByUser() async {
    return Requests.httpGET('/user/organizer');
  }
}