import 'dart:convert';

import 'package:event_mobile/Requests.dart';

class EventServices {
  static getEvents() async {
    return Requests.httpGET('/event');
  }

  static getEventById(id) async {
    return Requests.httpGET('/event/$id');
  }

  static deleteEventById(id) async {
    return Requests.httpDELETE('/event/$id');
  }

  static editEventById(id, title, description, location) async {
    var body = {
      "title": title,
      "description": description,
      "location": location
    };

    return Requests.httpPUT('/event/$id', body: jsonEncode(body));
  }

  static postEvent(var body) async {
    return await Requests.httpPOST('/event', body: jsonEncode(body));
  }
}

