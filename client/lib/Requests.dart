import 'dart:convert';
import 'dart:io';
import 'package:event_mobile/ApiVars.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import 'Controller/PortController.dart';

abstract class Requests {
  static Future buildHost() async {
    try {
      return await PortController.getHostPort();
    } catch(event) {
      return ApiVars.defaultIp;
    }
  }

  static Future<Map<String, String>> buildHeader() async {
    var pref = await SharedPreferences.getInstance();
    var token = pref.getString('token');
    return {
      HttpHeaders.contentTypeHeader: 'application/json',
      HttpHeaders.acceptHeader: 'application/json',
      HttpHeaders.authorizationHeader: 'Bearer ${token ?? ''}',
    };
  }

  static Future httpGET(String url) async {
    final response = await http.get(Uri.parse(await buildHost() + url), headers: await buildHeader());
    return jsonDecode(response.body);
  }

  static Future httpGETImage(String url) async {
    return await http.get(Uri.parse(await buildHost() + url), headers: await buildHeader());
  }

  static Future httpPOST(String url, { dynamic body }) async => body != null
    ? await http.post(Uri.parse(await buildHost() + url), headers: await buildHeader(), body: body)
    : await http.post(Uri.parse(await buildHost() + url), headers: await buildHeader());

  static Future httpPUT(String url, { dynamic body }) async {
    var response = await http.put(Uri.parse(await buildHost() + url), headers: await buildHeader(), body: body);
    return response.statusCode;
  }

  static Future httpDELETE(String url) async {
    var response = await http.delete(Uri.parse(await buildHost() + url), headers: await buildHeader());
    return response.statusCode;
  }
}