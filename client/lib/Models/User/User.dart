import 'package:json_annotation/json_annotation.dart';

part 'User.g.dart';

@JsonSerializable()

class User {
  String id;
  String name;
  String email;
  DateTime createdAt;
  bool? isOrganizer;
  String? college;

  User({
    required this.id,
    required this.name,
    required this.email,
    required this.createdAt,
    this.isOrganizer,
    this.college,
  });

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);

  Map<String, dynamic> toJson() => _$UserToJson(this);
}
