import 'package:json_annotation/json_annotation.dart';

import 'package:event_mobile/Models/User/User.dart';

part 'Organizer.g.dart';

@JsonSerializable()

class Organizer {
  User user;
  String id;
  DateTime createdAt;

  Organizer({
    required this.user,
    required this.id,
    required this.createdAt,
  });

  factory Organizer.fromJson(Map<String, dynamic> json) => _$OrganizerFromJson(json);

  Map<String, dynamic> toJson() => _$OrganizerToJson(this);
}
