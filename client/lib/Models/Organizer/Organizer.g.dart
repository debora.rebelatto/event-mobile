// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Organizer.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Organizer _$OrganizerFromJson(Map<String, dynamic> json) {
  return Organizer(
    user: User.fromJson(json['user'] as Map<String, dynamic>),
    id: json['id'] as String,
    createdAt: DateTime.parse(json['createdAt'] as String),
  );
}

Map<String, dynamic> _$OrganizerToJson(Organizer instance) => <String, dynamic>{
      'user': instance.user,
      'id': instance.id,
      'createdAt': instance.createdAt.toIso8601String(),
    };
