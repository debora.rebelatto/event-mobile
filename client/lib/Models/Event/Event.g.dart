// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Event.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Event _$EventFromJson(Map<String, dynamic> json) {
  return Event(
    title: json['title'] as String,
    description: json['description'] as String,
    city: json['city'] as String,
    state: json['state'] as String,
    location: json['location'] as String,
    initialDate: json['initialDate'] as String,
    finalDate: json['finalDate'] as String,
    organizer: Organizer.fromJson(json['organizer'] as Map<String, dynamic>),
    quantityTickets: json['quantityTickets'] as String,
    isFree: json['isFree'] as bool,
    price: json['price'] as int,
    participant:
        Participant.fromJson(json['participant'] as Map<String, dynamic>),
    createdAt: DateTime.parse(json['createdAt'] as String),
    id: json['id'] as String,
  );
}

Map<String, dynamic> _$EventToJson(Event instance) => <String, dynamic>{
      'title': instance.title,
      'description': instance.description,
      'city': instance.city,
      'state': instance.state,
      'location': instance.location,
      'initialDate': instance.initialDate,
      'finalDate': instance.finalDate,
      'organizer': instance.organizer,
      'quantityTickets': instance.quantityTickets,
      'isFree': instance.isFree,
      'price': instance.price,
      'participant': instance.participant,
      'createdAt': instance.createdAt.toIso8601String(),
      'id': instance.id,
    };
