import 'package:json_annotation/json_annotation.dart';

import 'package:event_mobile/Models/Organizer/Organizer.dart';
import 'package:event_mobile/Models/Participant/Participant.dart';

part 'Event.g.dart';

@JsonSerializable()

class Event {
  String title;
  String description;
  String city;
  String state;
  String location;
  String initialDate;
  String finalDate;
  Organizer organizer;
  String quantityTickets;
  bool isFree;
  int price;
  Participant participant;
  DateTime createdAt;
  String id;

  Event({
    required this.title,
    required this.description,
    required this.city,
    required this.state,
    required this.location,
    required this.initialDate,
    required this.finalDate,
    required this.organizer,
    required this.quantityTickets,
    required this.isFree,
    required this.price,
    required this.participant,
    required this.createdAt,
    required this.id,
  });

  factory Event.fromJson(Map<String, dynamic> json) => _$EventFromJson(json);

  Map<String, dynamic> toJson() => _$EventToJson(this);
}
