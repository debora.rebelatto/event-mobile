import 'package:json_annotation/json_annotation.dart';

import 'package:event_mobile/Models/Event/Event.dart';

part 'Participant.g.dart';

@JsonSerializable()

class Participant {
  String id;
  Event event;
  DateTime createdAt;

  Participant({
    required this.id,
    required this.event,
    required this.createdAt,
  });

  factory Participant.fromJson(Map<String, dynamic> json) => _$ParticipantFromJson(json);

  Map<String, dynamic> toJson() => _$ParticipantToJson(this);
}
