import 'package:event_mobile/Services/EventServices.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class EditEvent extends StatefulWidget {
  final event;

  EditEvent(this.event);

  @override
  _EditEventState createState() => _EditEventState();
}

class _EditEventState extends State<EditEvent> {
  TextEditingController _title = TextEditingController(text: '');
  TextEditingController _description = TextEditingController(text: '');
  TextEditingController _location = TextEditingController(text: '');
  DateTime datetime = DateTime.now();
  final format = DateFormat("HH:mm");


  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Editar Evento')),
      body: Container(
        padding: EdgeInsets.all(20),
        child: Column(
          children: [
            Container(
                padding: EdgeInsets.only(top: 10),
                child: TextFormField(
                  controller: _title,
                  decoration: InputDecoration(
                    hintText: 'Título'
                  ),
                  textInputAction: TextInputAction.next,
                  onEditingComplete: () => FocusScope.of(context).nextFocus(),
                ),
              ),

              Container(
                padding: EdgeInsets.only(top: 10),
                child: TextFormField(
                  controller: _location,
                  decoration: InputDecoration(
                    hintText: 'Localização'
                  ),
                  textInputAction: TextInputAction.next,
                  onEditingComplete: () => FocusScope.of(context).nextFocus(),
                ),
              ),

              Container(
                padding: EdgeInsets.only(top: 10),
                child: TextFormField(
                  maxLines: 3,
                  controller: _description,
                  decoration: InputDecoration(
                    hintText: 'Descrição'
                  ),
                  textInputAction: TextInputAction.next,
                  onEditingComplete: () => FocusScope.of(context).nextFocus(),
                ),
              ),
          ],
        ),
      ),

      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          var res = await EventServices.editEventById(
            widget.event['_id'],
            _title.text,
            _description.text,
            _location.text
          );

          if(res == 200) {
            Navigator.pop(context, true);
          } else {
            print('not edited');
          }
        },
        child: Icon(Icons.save),
      ),
    );
  }
}