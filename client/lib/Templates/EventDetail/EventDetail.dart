import 'package:event_mobile/Services/EventServices.dart';
import 'package:event_mobile/Services/ParticipantServices.dart';
import 'package:event_mobile/Templates/EditEvent/EditEvent.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../Components/currencyFormat.dart';

// ignore: must_be_immutable
class EventDetail extends StatefulWidget {
  var eventId;

  EventDetail(this.eventId);

  @override
  _EventDetailState createState() => _EventDetailState();
}

class _EventDetailState extends State<EventDetail> {
  bool _isOrganizer = false;
  bool isParticipating = false;
  var event;

  @override
  void initState() {
    super.initState();
    _getData();
  }

  _getData() async {
    var pref = await SharedPreferences.getInstance();
    var res = await EventServices.getEventById(widget.eventId);
    var id = pref.getString('userId');

    setState(() {
      event = res;
      _isOrganizer = id == event['organizer']['_id'] ? true : false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(appBar: AppBar(title: Text('Detalhes'),),
      body: event != null ? Container(
        padding: EdgeInsets.all(20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(children: [
              Text(event['title'], style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold)),
              edit(),
              delete()
            ],),
            SizedBox(height: 10),
            Text('${currencyFormat.format(event['value'])}', style: TextStyle(fontSize: 18)),
            Text('Data: ' + event['date'], style: TextStyle(fontSize: 18)),
            Text('Localização: ' + event['location'], style: TextStyle(fontSize: 18)),
            Text('Organizador: ' + event['organizer']['name'], style: TextStyle(fontSize: 18)),
            Text('Participantes: ${event['participants'].length}', style: TextStyle(fontSize: 18)),
            participateInEventButton()
          ]
        ),
      ) : Center(child: Container(child: CircularProgressIndicator(),),)
    );
  }

  edit() {
    return _isOrganizer ? IconButton(
      icon: Icon(Icons.edit),
      onPressed: () async {
        final result = await Navigator.of(context).push(MaterialPageRoute(builder: (context) => EditEvent(event['_id'])));
        if(result == true) {
          _getData();
        }
      },
    ) : Container();
  }

  delete() {
    return _isOrganizer ? IconButton(
      icon: Icon(Icons.delete),
      color: Colors.red,
      onPressed: () async {
        /* showDialog(
          context: context,
          child: AlertDialog(
            title: Text('Tem certeza que deseja deletar?'),
            actions: [
              FlatButton(
                child: Text('Sim'),
                onPressed: () async {
                  var res = await EventServices.deleteEventById(event['_id']);
                  if(res == 200) {
                    Navigator.of(context).pop();
                  }
                },
              ),
              FlatButton(
                child: Text('Cancelar'),
                onPressed: () async {
                  Navigator.of(context).pop();
                },
              )
            ],
          )
        ); */
      }
    ) : Container();
  }


  participateInEventButton() {
    return isParticipating ? Container(
      padding: EdgeInsets.all(10),
      width: MediaQuery.of(context).size.width,
      child: Text('Você já está participando')
    )
    : Container(
      padding: EdgeInsets.all(10),
      width: MediaQuery.of(context).size.width,
      child: TextButton(

        style: TextButton.styleFrom(
          backgroundColor: _isOrganizer
            ? Colors.grey[400] : Colors.deepPurpleAccent[700],
        ),
        child: Text(_isOrganizer ? 'Você é o organizador deste evento' : 'Participar',
          style: TextStyle(color: _isOrganizer ? Colors.black : Colors.white, )
        ),
        onPressed: _isOrganizer ? null : () async {
          Response res = await ParticipantServices.postParticipant(event['_id']);
          if(res.statusCode == 200) {
            setState(() {
              isParticipating = true;
            });
          }
        },
      )
    );
  }
}