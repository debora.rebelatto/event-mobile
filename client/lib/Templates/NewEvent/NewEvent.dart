import 'dart:convert';
import 'package:event_mobile/Templates/Components/Validators.dart';
import 'package:intl/intl.dart';

import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:event_mobile/Services/EventServices.dart';
import 'package:event_mobile/Templates/Homepage/FutureHomepage.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';

import 'NewField.dart';

class NewEvent extends StatefulWidget {
  @override
  _NewEventState createState() => _NewEventState();
}

class _NewEventState extends State<NewEvent> {
  TextEditingController _title = TextEditingController(text: 'Novo evento');
  TextEditingController _description = TextEditingController(text: 'isso é um evento!');
  TextEditingController _price = TextEditingController(text: '5.00');
  TextEditingController _location = TextEditingController(text: 'UFFS');
  TextEditingController _city = TextEditingController(text: 'CH');
  TextEditingController _state = TextEditingController(text: 'SC');
  bool isFree = false;
  DateTime? initialDate = DateTime.now();
  DateTime finalDate = DateTime.now();

  final format = DateFormat("dd/MM/yyyy hh:mm");

  bool waiting = false;
  final _formKey = GlobalKey<FormState>();
  var error;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Novo Evento'),),
      body: Container(
        padding: EdgeInsets.all(20),
        child: Form(
          key: _formKey,
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [


                NewFields(controller: _title, label: 'Titulo',),
                NewFields(controller: _description, label: 'Descrição'),
                NewFields(controller: _city, label: 'Cidade'),
                NewFields(controller: _state, label: 'Estado'),
                NewFields(controller: _location, label: 'Localização'),

                SizedBox(height: 20,),

                Text("Evento tem custos?"),

                CheckboxListTile(
                  title: Text("Is free"),
                  value: isFree,
                  onChanged: (newValue) {
                    setState(() {
                      isFree = !isFree;
                    });
                  },
                  controlAffinity: ListTileControlAffinity.leading,
                ),

                NewFields(controller: _price, label: 'Preço', isFree: isFree,),

                SizedBox(height: 20,),

                Text("Data de início"),

                Container(
                  padding: EdgeInsets.only(top: 10),
                  child: DateTimeField(
                    validator: (value) => FieldValidator.validateIfEmpty(value),
                    format: format,
                    onShowPicker: (context, currentValue) async {
                      final date = await showDatePicker(
                        context: context,
                        firstDate: DateTime.now(),
                        initialDate: currentValue ?? DateTime.now(),
                        lastDate: DateTime(2100)
                      );
                      if (date != null) {
                        final time = await showTimePicker(
                          context: context,
                          initialTime: TimeOfDay.fromDateTime(currentValue ?? DateTime.now()),
                        );
                        return DateTimeField.combine(date, time);
                      } else {
                        setState(() {
                          initialDate = currentValue!;
                        });
                        return currentValue;
                      }
                    },
                  ),

                ),


                SizedBox(height: 20,),

                Text("Data final"),

                Container(
                  padding: EdgeInsets.only(top: 10),
                  child: DateTimeField(
                    validator: (value) => FieldValidator.validateIfEmpty(value),
                    format: format,
                    onShowPicker: (context, currentValue) async {

                      final date = await showDatePicker(
                        context: context,
                        firstDate: initialDate ?? DateTime.now(),
                        initialDate: initialDate ?? DateTime.now(),
                        lastDate: DateTime(2100)
                      );
                      if (date != null) {
                        final time = await showTimePicker(
                          context: context,
                          initialTime: TimeOfDay.fromDateTime(currentValue ?? DateTime.now()),
                        );
                        return DateTimeField.combine(date, time);
                      } else {
                        setState(() {
                          initialDate = currentValue!;
                        });
                        return currentValue;
                      }
                    },
                  ),

                ),

                SizedBox(height: 10),

                Center(
                  child: Container(
                    width: MediaQuery.of(context).size.width * 0.75,
                    constraints: BoxConstraints(maxWidth: 300),
                    child: TextButton(
                      style: TextButton.styleFrom(
                        backgroundColor: Colors.white,
                        shape: RoundedRectangleBorder( borderRadius: BorderRadius.circular(40) ),
                      ),
                      child: waiting != false
                        ? Center(child: SizedBox(height: 20.0, width: 20.0, child: CircularProgressIndicator()))
                        : Text( 'Criar Evento', style: TextStyle( color: Colors.black87 ) ),
                      onPressed: waiting == true ? null : pressedButton
                    ),
                  ),
                )
              ],
            )
          )
        ),
      )
    );
  }

  pressedButton() async {
    if (_formKey.currentState!.validate()) {
      setState(() { waiting = !waiting; });

      var body = {
        'title': _title.text,
        'description': _description.text,
        'city': _city.text,
        'state': _state.text,
        'location': _location.text,
        "isFree": isFree,
        'price': _price.text,
        'initialDate': initialDate,
        'finalDate': finalDate,
      };

      Response response = await EventServices.postEvent(body);


      print(jsonEncode(response));

      /*

      setState(() { waiting = !waiting; });

      if(response.statusCode == 200 ) {
        Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) => FutureHomePage()));
      } else {
        setState(() { error = jsonDecode(response.body)['error'] ?? ''; });
      } */
    }
  }
}