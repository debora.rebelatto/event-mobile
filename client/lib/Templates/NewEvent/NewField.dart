import 'package:flutter/material.dart';

import 'package:event_mobile/Templates/Components/Validators.dart';

class NewFields extends StatefulWidget {
  final label;
  final TextEditingController controller;
  final bool? isFree;

  const NewFields({
    Key? key,
    required this.controller,
    this.isFree,
    this.label,
  }) : super(key: key);

  @override
  _NewFieldsState createState() => _NewFieldsState();
}

class _NewFieldsState extends State<NewFields> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 10),
      child: TextFormField(
        enabled: widget.isFree != true ? true : false,
        controller: widget.controller,
        decoration: InputDecoration(
          labelText: widget.label,
          hintText: widget.label,
          hintStyle: TextStyle(color: widget.isFree == true ? Colors.grey : null)
        ),
        textInputAction: TextInputAction.next,
        onEditingComplete: () => FocusScope.of(context).nextFocus(),
        validator: (value) => widget.isFree != true ? FieldValidator.validateIfEmpty(value) : null
      ),
    );
  }
}