import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import '../Components/currencyFormat.dart';

import 'package:event_mobile/Templates/EventDetail/EventDetail.dart';


class CardHomePage extends StatefulWidget {
  final event;

  CardHomePage(this.event);

  @override
  _CardHomePageState createState() => _CardHomePageState();
}

class _CardHomePageState extends State<CardHomePage> {
  var newFormat = DateFormat("dd/MM/yy hh:mm");
  late String updatedDt;

  @override
  void initState() {
    super.initState();
    // print(widget.event['date']);
    // updatedDt = newFormat.format(DateTime.parse(widget.event['date']));
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.fromLTRB(10, 2, 10, 2),
      child: Card(
        child: Ink(
          child: InkWell(
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => EventDetail(widget.event['_id'])
              ));
            },
            child: Container(
              padding: EdgeInsets.all(10),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(widget.event['title'], style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),),
                  // Text(updatedDt),
                  Text('${currencyFormat.format(widget.event['value'])}'),
                  Text(widget.event['location']),

                ],
              )
            ),
          )
        )
      )
    );
  }
}