import 'package:event_mobile/Models/Event/Event.dart';
import 'package:event_mobile/Templates/NewEvent/NewEvent.dart';
import 'package:flutter/material.dart';

import 'package:event_mobile/Templates/Drawer/LeftDrawer.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'CardHomePage.dart';

// ignore: must_be_immutable
class MyHomePage extends StatefulWidget {
  List<Event> eventList;

  MyHomePage(this.eventList, { Key? key }) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  bool isOrganizer = false;

  @override
  void initState() {
    super.initState();
    _getData();
  }

  _getData() async {
    var pref = await SharedPreferences.getInstance();
    setState(() {
      isOrganizer = pref.getBool('isOrganizer')!;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: LeftDrawer(),
      appBar: AppBar(title: Text('Homepage')),
      body: widget.eventList.isEmpty
      ? Center(child: Text('Não há eventos disponíveis!'))
      : Container(
        height: 10,
        padding: EdgeInsets.all(10),
        child: ListView.builder(
          shrinkWrap: true,
          itemCount: widget.eventList.length,
          itemBuilder: (context, index) {
            print('lkassjdasd');
            return Container();
          }
          /* CardHomePage(widget.eventList[index]) */
        ),
      ),
      floatingActionButton: isOrganizer ? FloatingActionButton(
        onPressed: () {
          Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) => NewEvent()));
        },
        child: Icon(Icons.add),
      ) : SizedBox(height: 0)
    );
  }
}
