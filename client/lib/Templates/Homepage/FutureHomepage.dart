import 'package:event_mobile/Models/Event/Event.dart';
import 'package:event_mobile/Services/EventServices.dart';
import 'package:event_mobile/Templates/Homepage/HomePage.dart';
import 'package:flutter/material.dart';

class FutureHomePage extends StatefulWidget {
  @override
  _FutureHomePageState createState() => _FutureHomePageState();
}

class _FutureHomePageState extends State<FutureHomePage> {
  late Future<dynamic> _future;

  @override
  void initState() {
    super.initState();
    _future = _getData();
  }

  _getData() {
    return EventServices.getEvents();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<dynamic>(
      future: _future,
      builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
        var children;

        if (snapshot.hasData) {
          List<Event> events = snapshot.data;
          children = MyHomePage(events);
        } else if (snapshot.hasError) {
          print('err');
          children = Text('${snapshot.error}');
        } else {
          children = CircularProgressIndicator();
        }
        return children;

      },
    );
  }
}