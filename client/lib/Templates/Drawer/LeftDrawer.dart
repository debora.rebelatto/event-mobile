import 'package:event_mobile/Controller/SharedPrefController.dart';
import 'package:event_mobile/Templates/Homepage/FutureHomepage.dart';
import 'package:event_mobile/Templates/HostConfig/HostConfig.dart';
import 'package:flutter/material.dart';
import 'package:jwt_decoder/jwt_decoder.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'DrawerItem.dart';
import 'UserDetail.dart';

class LeftDrawer extends StatefulWidget {
  LeftDrawer({ Key? key }) : super(key: key);
  @override
  _DrawerState createState() => _DrawerState();
}

class _DrawerState extends State<LeftDrawer> {
  bool logged = false;

  @override
  void initState() {
    super.initState();
    _getLogged();
  }

  dynamic _getLogged() async {
    var pref = await SharedPreferences.getInstance();
    var token = pref.getString('token');

    if(JwtDecoder.isExpired(token!)) {
      await SharedPrefController.removeUser();
      setState(() {
        logged = false;
      });
    } else {
      setState(() {
        logged = true;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(children: [
        Expanded( child: ListView(
          physics: BouncingScrollPhysics(),
          children: <Widget>[
            UserDetail(),
            logged ? DrawerItem(Icon(Icons.home_outlined), 'Página Inicial', route: FutureHomePage()) : Container(),
            Container(padding: EdgeInsets.only(left: 10, right: 10), child: Divider(color: Colors.grey)),
            logged ? DrawerItem(Icon(Icons.exit_to_app), 'Sair') : Container(),
            DrawerItem(Icon(Icons.settings_outlined), 'Configurações Host', route: HostPorta()),
          ],
        )),
        Container(padding: EdgeInsets.only(left: 10, right: 10), child: Divider(color: Colors.grey)),
      ],)
    );
  }
}
