import 'package:event_mobile/Controller/SharedPrefController.dart';
import 'package:event_mobile/Templates/InitialScreen/InitialScreen.dart';
import 'package:flutter/material.dart';

class DrawerItem extends StatelessWidget {
  final Icon _icon;
  final String _name;
  final Widget? route;
  DrawerItem(this._icon, this._name, { this.route });


  @override
  Widget build(BuildContext context) => ListTile(
    leading: _icon,
    title: Text(_name),
    onTap: () async {
      if(route != null) {
        Navigator.pop(context);
        await Navigator.of(context).push(MaterialPageRoute(builder: (context) => route!));
      } else {
        Navigator.pop(context);
        await SharedPrefController.removeUser();
        await Navigator.of(context).push(
          MaterialPageRoute(builder: (BuildContext context) => InitialScreen()),
        );
      }
    },
  );
}