import 'package:event_mobile/Templates/Profile/WidgetProfile.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UserDetail extends StatefulWidget {
  @override
  UserDetailState createState() => UserDetailState();
}

class UserDetailState extends State<UserDetail>{
  var name = '';
  var email = '';

  @override
  void initState() {
    super.initState();
    _getUser();
  }

  _getUser() async {
    var pref = await SharedPreferences.getInstance();
      var userName = pref.getString('name');
      var userEmail = pref.getString('email');

    setState(() {
      name = userName!;
      email = userEmail!;
    });
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        child: UserAccountsDrawerHeader(
          decoration: BoxDecoration( color: Theme.of(context).bottomAppBarColor),
          currentAccountPicture: Container(
            padding: EdgeInsets.only(bottom: 10),
            child: CircleAvatar(
              radius: 30,
              backgroundColor: Colors.green[200],
              child: Text(name != '' ? '${name[0]}' : '', style: TextStyle(fontSize: 30.0, color: Colors.white ))
            )
          ),
          accountName: Container(
            padding: EdgeInsets.only( top: 30 ),
            child: Text( name != '' ? name : 'Login', style: TextStyle(color: Colors.black ),)
          ),

          accountEmail: Container(
            child: Text( email != '' ? email : 'Email', style: TextStyle(color: Colors.black ))
          )
        ),
        onTap: () {
          Navigator.of(context).push(MaterialPageRoute(builder: (context) => WidgetProfile()));
        },
      );
  }
}