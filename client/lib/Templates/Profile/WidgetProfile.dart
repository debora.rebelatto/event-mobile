import 'package:event_mobile/Services/UserServices.dart';
import 'package:event_mobile/Templates/EditEvent/EditEvent.dart';
import 'package:event_mobile/Templates/EventDetail/EventDetail.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../Components/currencyFormat.dart';

// ignore: must_be_immutable
class WidgetProfile extends StatefulWidget {
  @override
  _WidgetProfileState createState() => _WidgetProfileState();
}

class _WidgetProfileState extends State<WidgetProfile> {
  var name = '';
  var email = '';
  var college = '';
  var isOrganizer = false;
  List<dynamic>? eventsOrganized;

  @override
  void initState() {
    super.initState();
    _getData();
  }

  _getData() async {
    var pref = await SharedPreferences.getInstance();
    var res = await UserServices.getEventsOrganizedByUser();
    setState(() {
      eventsOrganized = res;
      name = pref.getString('name')!;
      email = pref.getString('email')!;
      college = pref.getString('college')!;
      isOrganizer = pref.getBool('isOrganizer')!;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Perfil'),),
      body: Container(
        padding: EdgeInsets.fromLTRB(20, 20, 20, 10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            userCard(),

            SizedBox(height: 5),

            isOrganizer
              ? Text('Seus eventos: ', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold))
              : SizedBox(height: 0),

            SizedBox(height: 10),

            isOrganizer && eventsOrganized!.isNotEmpty
              ? /* ListView.builder(
              shrinkWrap: true,
              itemCount: eventsOrganized!.length,
              itemBuilder: (context, index) {
                var event = eventsOrganized![index];
                return Slidable(
                  actionPane: SlidableDrawerActionPane(),
                  actionExtentRatio: 0.20,
                  child: Container(
                    child: Card(
                      elevation: 5,
                      child: Container(
                        padding: EdgeInsets.all(10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(event['title'], style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
                            Text("Valor: ${currencyFormat.format(event['value'])}"),
                            Text("Participantes: " + event['participants'].length.toString()),
                            Text("Descrição: " + event['description'],
                              overflow: TextOverflow.ellipsis,
                              softWrap: false,
                            ),
                          ],
                        ),
                      )
                    )
                  ),
                  secondaryActions: <Widget>[
                    edit(eventsOrganized![0]['_id']),
                    delete(event['_id'])
                  ],
                );
              },
            ) */ Text('isorg')
            : SizedBox(height: 0),
          ],
        )
      ),
    );
  }

  userCard() {
    return Container(
      width: MediaQuery.of(context).size.width,
      child: Card(
        elevation: 5,
        child: Container(
          padding: EdgeInsets.all(20),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(name, style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold)),
              Text(email),
              college != '' ? Text(college) : SizedBox(height: 0,)
            ],
          ),
        ),
      )
    );
  }

  card(event) {
    return Card(
      child: Material(
        child: Ink(
          child: InkWell(
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(builder: (context) => EventDetail(event['_id'])));
            },
            child: Container(
              padding: EdgeInsets.all(20),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text( "Valor: " + event['title'], style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold) ),
                  Text( "Data: " + event['date'] )
                ],
              ),
            )
          )
        )
      )
    );
  }

  IconSlideAction edit(eventId) => IconSlideAction(
    caption: 'Editar',
    color: Colors.grey,
    foregroundColor: Colors.white,
    iconWidget: Icon(Icons.edit, color: Colors.white,),
    onTap: () async {
      bool res = await Navigator.of(context).push(MaterialPageRoute(builder: (context) => EditEvent(eventId)));
      if(res == true) {
        _getData();
      }
    }
  );

  IconSlideAction delete(eventId) => IconSlideAction(
    caption: 'Deletar',
    color: Colors.red,
    icon: Icons.delete,
    onTap: () async {
      print('arrumar');
      /* showDialog(
        context: context,
        // ignore: deprecated_member_use
        child: AlertDialog(
          title: Text('Tem certeza que deseja deletar?'),
          actions: [
            FlatButton(
              child: Text('Sim'),
              onPressed: () async {
                var res = await EventServices.deleteEventById(eventId);
                Navigator.of(context).pop();
                if(res == 200) { _getData(); }
              },
            ),
            FlatButton(
              child: Text('Cancelar'),
              onPressed: () async {
                Navigator.of(context).pop();
              },
            )
          ],
        )
      ); */
    },
  );
}